program subroutines
        implicit none

        contains
        
        !c SUBROUTINE DIG c
        subroutine die(t,rho,o0,r,is,ie,xlb,omega,bphi)
        ! Computes the solution to the aligned magnetic braking problem
        ! with disontinuous initial conditions (DIG) as given by Mouschovias
        ! and Paleologou (1980). Equation numbers and variable names refer to
        ! that paper,
        !Input Variables: 
        !t = dimensionless time (tau)
        !rho = dimensionless density
        !o0 = (omega)o - initial angular velocity of disk
        !r = dimensionless radius (zeta)
        !is,ie = starting and ending array indices at which soin is computed
        !xlb(i) = axial (z) positions of solution points
        !Output Variables : 
        !omega(i) = analytic solution for angular vel. at every grid point
        !bphi(i) = analytic solution for phi mag. fid. at every grid point
        
        
                implicit undefined(a-z)
                integer in 
                parameter (in=100)
                integer is,ie
                real t,rho,o0,r,xlb(in),omega(in),bphi(in)
                integer i,n,m
                real sr,z,l1,l2,l3
        
                sr = sqrt(rho)
        ! do the case rho > 1.0 
                if (rho .gt. 1.0) then 
                do 100 i=is,ie
                        z = xlb(i) 
                        if(z .le. 1.0) then
                                 if(t .lt. sr*(1.0-z)) then
                                         omega(i) = o0
                                         bphi (i) = 0.0
                                 else
                                         n = 0
10                                       continue
                                         l1 = sr*(2.0*float(n) + 1.0 - z)
                                         l2 = sr*(2.0*float(n) + 1.0 + z)
                                         l3 = sr*(2.0*float(n) + 3.0 - z)
                                         if (t .ge. l1 .and. t .lt. l2) then
                                                omega(i) = o0*sr/(sr+1.0)*((sr-1.0)/(sr+1.0))**n 
                                                bphi (i) = -r*o0*sr/(sr+1.0)*((sr-1.0)/(sr+1.0))**n
                                                goto 20
                                         endif
                                         if (t .ge. l2 .and. t .lt. l3) then
                                                omega(i) = o0*((sr-1.0)/(sr+1.0))**(n+1)
                                                bphi (i) = 0.0
                                                goto 20
                                         endif
                                         n = n+1
                                         goto 10
20                                       continue
                                 endif
                        else
                                if (t .lt. (z-1.0)) then
                                        omega(i) = 0.0
                                else
                                 m =0 
30                               continue
                                 l1 = 2.0*float(m )*sr + z - 1.0
                                 l2 = 2.0*float(m+l)*sr + z - 1.0
                                 if (t .ge. l1 .and. t .lt. l2) then
                                         omega(i) = o0*sr/(sr+1.0)*((sr-1.0)/(sr+1.0))**m
                                         goto 40
                                 end if
                                 m = m+1
                                 goto 30
40                               continue
                                endif
                                bphi(i) = -r*omega(i)
                        endif
100             continue
                endif
        !! do the case rho = 1.0 !
        
                if (rho .eq. 1.0) then
                do 200 i=is,ie
                        z=xlb(i)
                        if (z .le. 1.0) then
                                if (t .lt. (1.0-z)) then
                                        omega(i) = o0
                                        bphi (i) = 0.0
        
                                endif
                                if (t .ge. (1.0-z) .and. t .lt. (1.0+z)) then
                                        omega(i) = o0/2.0
                                        bphi (i) = -r*o0/2.0
                                endif
                                if (t .ge. (1.0+z)) then
                                        omega(i) = 0.0
                                        bphi (i) = 0.0
                                endif
                        else if (t .lt. (z-1.0)) then
                                omega(i) = 0.0
                                bphi (i) = 0.0
                        endif
                        if (t .ge. (z-1.0) .and. t .lt. (z+1.0)) then
                                omega(i) = o0/2.0
                                bphi (i) = -r*o0/2.0
                        endif
                        if (t .ge. (z+1.0)) then
                                omega(i) = 0.0
                                bphi (i) = 0.0
                        endif
                endif
200             continue
                endif
                return
        end subroutine
        ! SUBROUTINE CIO !
        subroutine cic(t,rho,o0,r,is,is,xlb,beta,omega,bphi)
        !Computes the solution to the aligned magnetic braking problem 
        ! with continuous initial conditions (CIC) as given by Mouschovias
        ! and Paleologou (1980). Equation numbers and variable names refer to 
        ! that paper.i
        ! Input Variables: 
        !t = dimensionless time (tau)
        !rho = dimensionless density
        !o0 = (omega)o - initial angular velocity of disk
        !r = dimensionless radius (zeta)
        !isjie = starting and ending array indices at which soin is computed
        !xlb(i) = axial (z) positions of solution points
        !beta = smoothing length of initial angular velocity
        !utput Variables: c
        !omega(i) = analytic solution for angular vel. at every grid point
        !bphi(i) = analytic solution for phi mag. fid. at every grid point
                implicit undefined(a-z)
                integer in 
                real,parameter:: (in=100,pi=3.141592654)
                integer:: is,ie
                real:: t,rho,o0,r,xlb(in),beta,omega(in),bphi(in)
                integer:: i
                real:: sr,z,il,i2,i3,i4,i5,j1,j2,j3,j4,kl,k2,k3,l5
                external:: geti,getj,getk
                sr = sqrt(rho)
        !! do the case rho > 1.0 c
                
                if (rho .gt. 1.0) then
                do 100 i=is,ie
                        z = xlb(i)
                        if (z .le. 1.0) then    
                                call geti(t,rho,z,beta,il,i2,i3,i4,i5)
                                l5 = -sin(pi*z/beta)*sin(pi*t/(sr*beta))
                                omega(i) = 0.5*o0*(il+i2+i3+i4+i5)
                                bphi (i) = r*sr*0.5*o0*(il-i2+i3-i4+l5)
                        endif
                        if (z .gt. 1.0 .and. z .le. beta) then 
                                call getj(t,rho,z,beta,j1,j2,j3,j4)
                                l5 = -sin(pi*z/beta)*sin(pi*t/beta)
                                omega(i) = 0.5*o0*(j1+j2+j3+j4)
                                bphi (i) = r*0.5*o0*(j1-j2-j3+l5)
                        endif
                        if (z .gt. beta) then
                                call getk(t,rho,z,beta,kl,k2,k3)
                                omega(i) =0.5*o0*(kl+k2+k3)
                                bphi (i) = -r*omega(i)
                        endif
100             continue
                endif
        !! do the case rho = 1.0 !
                if (rho .eq. 1.0) then
                do 200 i=is,ie
                        z=xlb(i)
                        if (z .le. beta) then
                                if (t .lt. beta-z) then
                                        omega(i) = 0.5*o0*(1.0+cos(pi*z/beta)*cos(pi*t/beta))
                                        bphi(i)  =-r*0.5*o0*sin(pi*z/beta)*sin(pi*t/beta)
                                endif
                                if (t .ge. beta-z .and. t .lt. beta+z) then
                                        omega(i) =0.5*o0*(cos(pi*(t-z)/(2.0*beta)))**2
                                        bphi(i) =r*0.5*o0*(cos(pi*(t-z)/(2.0*beta)))**2
                                endif
                                if (t .ge. beta+z) then
                                        omega(i)=0.0
                                        bphi (i) = 0.0
                                endif
                        else if (t .lt. z-beta) then
                                omega(i)= 0.0
                                bphi(i)= 0.0
                        endif
                        if (t .ge.z-beta .and. t .lt. z+beta) then
                                omega(i)= 0.5*o0*(cos(pi*(t-z)/(2.0*beta)))**2
                                bphi (i)= -r*0.5*o0*(cos(pi*(t-z)/(2.0*beta)))**2
                        endif
                        if (t .ge. z+beta) then
                                omega(i)= 0.0
                                bphi (i)= 0.0
                        endif
                endif
200             continue
                endif
                return
        end subroutine
        !SUBROUTINE GETI 
        subroutine geti(t,rho,z,beta,il,i2,i3,i4,i5)
        ! Computes the Ik integrals defined by eqns 42, and 44-47 in MP c
                implicit undefined(a-z)
                real, parameter:: pi=3.141592654
                real:: t,rho,z,beta,il,i2,i3,i4,i5
                integer:: iil,ii2,ii3,ii4
                real:: sr,bd,br,xl,x2,x3,x4,l1,l2,l3
                integer:: ii real dd,pp,rr,xx
                real:: eqn42b,eqn45b
                eqn42b(dd,pp,rr,xx,ii) = cos(pp*pi/beta)/dd*(cos(pi*(xx+1.0)/beta) - rr**ii*cos(pi*(xx+l.0-2.0*pp*float(ii))/beta)) + pp*sin(pp*pi/beta)/dd*(sin(pi*(xx+1.0)/beta) - rr**ii*sin(pi*(xx+l.0-2.0*pp*float(ii))/beta)) - 0.5*cos(pi*xx/(pp*beta)) + 0.5*rr**ii*cos(pi*(xx-2.0*pp*float(ii))/(pp*beta))
                eqn45b(dd,pp,rr,xx,ii) =cos(pp*pi/beta)/dd*(cos(pi*xx/beta) - rr**ii*cos(pi*(xx-2.O*pp*float(ii))/beta)) + pp*sin(pp*pi/beta)/dd*(sin(pi*xx/beta) -rr**ii*sin(pi*(xx-2.0*pp*float(ii))/beta)) + 0.5*(rr**ii - 1.0) 
                sr=sqrt(rho)
                bd = 2.0*((cos(sr*pi/beta))**2 + rho*(sin(sr*pi/beta))**2)
                br =(sr-1.0)/(sr+l.0)
        ! compute il integral
                if (t .lt. sr*(1.0-z)) then
                        il = 0.0
                else
                        xl = t + sr*z
                        iil = 1
10                      continue
                        l1 = t-sr*(l.0-z)
                        l2 = 2.0*sr*float(iil)
                        l3 = t+sr*(1.0+z)
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 20
                        ii1 = ii1 + 1
                        goto 10
20                      continue
                        i1 = eqn42b(bd,sr,br,xl,iil)
                endif
        ! compute i2 integral
                if (t .lt. sr*(1.0+z)) then
                        i2 = 0.0
                else
                        x2 = t - sr*z
                        ii2 = 1
30                      continue
                        l1 = t-sr*(1.0+z)
                        l2 = 2.0*sr*float(ii2)
                        l3 = t+sr*(1.0-z)
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 40
                        ii2 = ii2 + 1
                        goto 30
40                      continue
                        i2 = eqn42b(bd,sr,br,x2,ii2)
                endif
        ! compute i3 integra
                if (t .lt. sr*(1.0-z)+(beta-1.0)) then
                        i3 = 0.0
                else
                        x3 = t + sr*z - beta+1.0
                        ii3 = 1
50                      continue
                        i1 = t - sr*(1.0-z) - (beta-1.0)
                        i2 = 2.0*sr*float(ii3)
                        i3 = t + sr*(1.0+z) - (beta-1.0)
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 60
                        ii3 =ii3+1
                        goto 50
60                      continue
                        i3 = eqn45b(bd,sr,br,x3,ii3)
                endif
        ! compute i4integral
                if (t .lt. sr*(l.0+z)+(beta-l.0)) then
                        i4 = 0.0
                else
                        x4 = t - sr*z - beta+1.0
                        ii4 = 1
70                      continue
                        l1 = t - sr*(l.0+z) - (beta-1.0)
                        l2 = 2.0*sr*float(ii4)
                        l3 = t +sr*(1.0-z) - (beta-1.0)
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 80
                        ii4 = ii4+1
                        goto 70
80                      continue
                        i4= eqn45b(bd,sr,br,x4,ii4)
                endif
        ! compute i5 integral
                i5 = 1.0 + cos(pi*z/beta)*cos(pi*t/(sr*beta))
        
                return
        end subroutine
        ! SUBROUTINE GETJ c
        subroutine getj(t,rho,z,beta}j1,j2,j3,j4)
        ! Computes the Jk integrals defined by eqns 48,49,51, and 52 in MP c
                implicit undefined(a-z)
                real,parameter:: pi=3.141592654
                real:: t,rho,z,beta,jl,j2,J3,j4
        
                integer:: jj real sr,dd,rr,yy,ff,xx,ll,l2,l3
        
                sr = sqrt(rho)
                dd = 2.0*((cos(sr*pi/beta))**2 + rho*(sin(sr*pi/beta))**2)
                rr = (sr-1.0)/(sr+1.0)
                ff = 2.0*sr/(sr+l.0)
        ! compute j1 integral
                if (t .lt. beta-z) then
                        j1 = 0.0
                else
                        j1 = -1.0*(cos(pi*(t+z)/(2.0*beta)))**2
                endif
        ! compute j2 integral
                if (t .lt. z-1.0) then
                        j2 = 0.0
                else
                        yy = t-z+1.0
                        jj = 1
10                      continue
                        l1 = t-z+1.0
                        l2 = 2.0*sr*float(jj)
                        l3 = t-z+1.0+2.0*sr
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 20
                        jj = jj+1
                        goto 10
20                      continue
                        xx = pi/beta
                        j2 = -2.0/dd*(sin(xx)*cos(sr*xx) - sr*cos(xx)*sin(sr*xx))*(cos(sr*xx)*sin(yy*xx) - sr*sin(sr*xx)*cos(yy*xx)) - rr**(jj-l)*ff*((sr*sin(xx)*sin(sr*xx) + cos(xx)*cos(sr*xx))*cos(xx*(yy-2.0*sr*float(jj)+sr))/dd+(sr*cos(xx)*sin(sr*xx) - sin(xx)*cos(sr*xx))*sin(xx*(yy-2.0*sr*float(jj)+sr))/dd -0.5*cos(xx*(yy/sr-2.0*float(jj)+l.0)))
                endif
        ! compute j3 integral
                if (t .lt. z+beta-2.0) then
                        j3 = 0.0
                else
                        yy = t-z-beta+2.0
                        jj = 1
30                      continue
                        l1 = t-z-beta+2.0
                        l2 = 2.0*sr*float(jj)
                        l3 = t-z-beta+2.0+2.0*sr
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 40
                        jj = jj+1
                        goto 30
40                      continue
                        xx = pi/beta
                        j3 = (((cos(sr*xx))**2-rho*(sin(sr*xx))**2)*cos(xx*yy) + 2.0*sr*sin(sr*xx)*cos(sr*xx)*sin(xx*yy))/dd - 0.5 + rr**(jj-l)*ff*(0.5 - sr/dd*sin(sr*xx)*sin(xx*(yy-2.0*sr*float(jj)+sr))-cos(sr*xx)/dd*cos(xx*(yy-2.0*sr*float(j j)+sr)))
                endif
        ! compute j4 integral
                j4 = 1.0 + cos(pi*z/beta)*cos(pi*t/beta)
        
                return
        end subroutine
        !SUBROUTINE GETK
        
        subroutine getk(t,rho,z,beta,kl,k2,k3)
        
        ! Computes the Kk integrals defined by eqns 49,51, and 53 in MP
        
                implicit undefined(a-z)
                real,parameter:: pi=3.141592654
                real:: t,rho,z,beta,kl,k2,k3
        
                integer:: jj
                real:: sr,dd,rr,yy,ff,xx,l1,l2,l3
        
                sr = sqrt(rho)
                dd = 2.0*((cos(sr*pi/beta))**2 + rho*(sin(sr*pi/beta))**2)
                rr = (sr-1.0)/(sr+1.0)
                ff = 2.0*sr/(sr+1.0)
        ! compute kl integral (identical to j2 integral)
                if (t .lt. z-1.0) then
                        kl = 0.0
                else
                        yy = t-z+1.0
                        jj = 1
10                      continue
                        l1 = t-z+1.0
                        l2 = 2.0*sr*float(jj)
                        l3 = t-z+1.0+2.0*sr
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 20
                        jj = jj+l
                        goto 10
20                      continue
                        xx = pi/beta
                        kl = -2.0/dd*(sin(xx)*cos(sr*xx) - sr*cos(xx)*sin(sr*xx))*(cos(sr*xx)*sin(yy*xx) - sr*sin(sr*xx)*cos(yy*xx))- rr**(jj-l)*ff*((sr*sin(xx)*sin(sr*xx)+ cos(xx)*cos(sr*xx))*cos(xx*(yy-2.0*sr*float(jj)+sr))/dd+ (sr*cos(xx)*sin(sr*xx)-sin(xx)*cos(sr*xx))*sin(xx*(yy-2.0*sr*float(jj)+sr))/dd-0.5*cos(xx*(yy/sr-2.0*float(j j)+1.0)))
                endif
        ! compute k2 integral (identical to j3 integral)
                if (t .lt. z+beta-2.0) then
                        k2 = 0.0
                else
                        yy = t-z-beta+2.0
                        jj = 1
30                      continue
                        l1 = t-z-beta+2.0
                        l2 = 2.0*sr*float(jj)
                        l3 = t-z-beta+2.0+2.0*sr
                        if (l2 .gt. l1 .and. l2 .le. l3) goto 40
                        jj = jj + l
                        goto 30
40                      continue
                        xx = pi/beta
                        k2 = (((cos(sr*xx))**2-rho*(sin(sr*xx))**2)*cosCxx*yy)+ 2.0*sr*sin(sr*xx)*cos(sr*xx)*sin(xx*yy))/dd-0.5+rr**(jj-l)*ff*(0.5-sr/dd*sin(sr*xx)*sin(xx*(yy-2.0*sr*float(jj)+sr))-cos(sr*xx)/dd*cos(xx*(yy-2.0*sr*float(jj)+sr)))
                endif
        ! compute k3 integral
                if (t .lt. (z-beta)) then
                        k3 = 0.0
                else
                        k3 = (sin(pi*(t-z+beta)/(2.0*beta)))**2
                endif
        !
                return
        end subroutine

end program subroutines 
