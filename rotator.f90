program rotator
        use subrutines
        implicit none
        integer::is,ie
        real   ::t,rho,o0,r
        real,dimension(:), allocatable :: xlb,omega,bphi
        !character ::    
        ! extra varibales
        integer::n,i
        ! initialization of varibles
        !--------------------------
        is=0
        ie=100
        n=ie-is+1
        rho=1.0
        o0=1.0
        r=0.5
        allocate(xlb(n))
        do i=0,n
                xlb(i)=0.0+0.1*i*r
        enddo
        !-------------------------
        allocate(omega(n),bphi(n))

        call die(t,rho,o0,r,is,ie,xlb,omega,bphi)

        deallocate(xlb,omega,bphi)
end program rotator
